# Jenkins Pipeline with OpenShift

This repository contains an example on how to create Jenkins Pipelines to manage OpenShift deployments.

## Environment

OpenShift 4.x
Jenkins 2 using OCP template

## Case

Simple NodeJS HelloWorld application

## Create Projects
    oc new-project segregated-hml
    oc new-project segregated-prd
    oc new-project automation

## Add permition to Jenkins Service Account

You can allow Jenkins to have a cluster wide edit permission(only for testing scenarios)

    oc adm policy add-cluster-role-to-user edit system:serviceaccount:automation:jenkins

Or give access to project level

    oc policy add-role-to-user edit system:serviceaccount:automation:jenkins -n segregated-hml
    oc policy add-role-to-user edit system:serviceaccount:automation:jenkins -n segregated-prd
